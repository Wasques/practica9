Untitled
- Delay -
active: false
- Duration - 
lowMin: 99999.0
lowMax: 99999.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 250.0
highMax: 250.0
relative: false
scalingCount: 3
scaling0: 0.06741573
scaling1: 0.3988764
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.544592
timeline2: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 999.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.4117647
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.1369863
timeline2: 1.0
- Life Offset - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.21232876
timeline2: 0.47945204
timeline3: 1.0
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 50.0
lowMax: 50.0
highMin: 50.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 90.0
lowMax: 90.0
highMin: 45.0
highMax: 135.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.37254903
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.28082192
timeline2: 1.0
- Rotation - 
active: false
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.6627451
colors2: 0.14117648
colors3: 1.0
colors4: 0.54509807
colors5: 0.047058824
colors6: 1.0
colors7: 0.4627451
colors8: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.4698795
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.2631579
scaling1: 0.40350878
scaling2: 0.54385966
scaling3: 0.31578946
timelineCount: 4
timeline0: 0.0
timeline1: 0.28082192
timeline2: 0.65753424
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: true
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
fire.png
